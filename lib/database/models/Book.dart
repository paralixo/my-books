import 'package:my_books/database/models/BookImage.dart';

class Book {
  final int id;
  String title;
  String author;
  List<BookImage> images;

  Book({this.id, this.title, this.author});

  List<String> getAllPaths() {
    List<String> paths = [];
    if (images == null) return null;

    for (BookImage bookImage in images) {
      paths.add(bookImage.path);
    }

    return paths;
  }

  Map<String, dynamic> toMap() {
    return {'ID': id, 'TITLE': title, 'AUTHOR': author};
  }

  static Book mapToBook(Map<String, dynamic> map) {
    return Book(id: map['ID'], title: map['TITLE'], author: map['AUTHOR']);
  }

  static List<Book> mapToBooks(List<Map<String, dynamic>> maps) {
    List<Book> books = [];
    for (Map<String, dynamic> map in maps) {
      books.add(Book.mapToBook(map));
    }
    return books;
  }
}
