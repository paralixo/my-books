class BookImage {
  final int id;
  int bookId;
  String path;

  BookImage({this.id, this.bookId, this.path});

  Map<String, dynamic> toMap() {
    return {'ID': id, 'BOOK_ID': bookId, 'IMAGE_PATH': path};
  }

  static BookImage mapToBookImage(Map<String, dynamic> map) {
    return BookImage(
        id: map['ID'], bookId: map['BOOK_ID'], path: map['IMAGE_PATH']);
  }

  static List<BookImage> mapToBookImages(List<Map<String, dynamic>> maps) {
    List<BookImage> bookImages = [];
    for (Map<String, dynamic> map in maps) {
      bookImages.add(BookImage.mapToBookImage(map));
    }
    return bookImages;
  }
}
