import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:my_books/database/constants/tables.constants.dart';

class DBProvider {
  static final DBProvider _instance = DBProvider._internal();
  static Database database;

  factory DBProvider() {
    return _instance;
  }

  DBProvider._internal();

  Future init() async {
    if (database != null) {
      return;
    }
    database = await initDB();
  }

  Future initDB() async {
    String path = join(await getDatabasesPath(), 'database.db');
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      print("Creating new database with version $version");
      return await _createDB(db, version);
    }, onOpen: (Database db) async {
      print("Actual database version is " + (await db.getVersion()).toString());
    });
  }

  Future _createDB(Database db, int version) async {
    if (version > 1) {
      await db.execute("delete from $BOOKS_TABLE");
      await db.execute("delete from $BOOKS_IMAGES_TABLE");
    }
    await db.execute(
        "CREATE TABLE $BOOKS_TABLE(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, TITLE TEXT, AUTHOR TEXT);");
    await db.execute(
        "CREATE TABLE $BOOKS_IMAGES_TABLE(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, BOOK_ID INTEGER, IMAGE_PATH TEXT);");
  }
}
