import 'package:my_books/database/constants/tables.constants.dart';
import 'package:my_books/database/Database.dart';
import 'package:my_books/database/models/BookImage.dart';
import 'package:sqflite/sqflite.dart';

class BookImageRessource {
  Database db = DBProvider.database;

  BookImageRessource();

  Future<List<BookImage>> getBookImagesWithBookId(final int bookId) async {
    final List<Map<String, dynamic>> maps =
        await db.query(BOOKS_IMAGES_TABLE, where: "BOOK_ID = $bookId");
    return BookImage.mapToBookImages(maps);
  }

  Future<int> saveBookImage(BookImage book) async {
    return await db.insert(BOOKS_IMAGES_TABLE, book.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }
}
