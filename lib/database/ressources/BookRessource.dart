import 'package:my_books/database/constants/tables.constants.dart';
import 'package:my_books/database/Database.dart';
import 'package:my_books/database/models/Book.dart';
import 'package:my_books/database/models/BookImage.dart';
import 'package:my_books/database/ressources/BookImageRessource.dart';
import 'package:sqflite/sqflite.dart';

class BookRessource {
  Database _db = DBProvider.database;
  BookImageRessource _bookImageRessource = new BookImageRessource();

  BookRessource();

  Future<List<Book>> getBooks() async {
    final List<Map<String, dynamic>> maps = await _db.query(BOOKS_TABLE);
    List<Book> books = Book.mapToBooks(maps);

    for (Book book in books) {
      book.images = await _bookImageRessource.getBookImagesWithBookId(book.id);
    }

    return books;
  }

  Future<int> saveBook(Book book) async {
    final int bookId = await _db.insert(BOOKS_TABLE, book.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);

    if (book.images == null) return bookId;
    for (BookImage bookImage in book.images) {
      bookImage.bookId = bookId;
      await _bookImageRessource.saveBookImage(bookImage);
    }

    return bookId;
  }
}
