import 'package:flutter/material.dart';
import 'package:my_books/views/books/Books.dart';
import 'package:my_books/views/home/Home.dart';
import 'package:my_books/views/books/Wishes.dart';
import 'package:my_books/database/Database.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await DBProvider().init();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: App());
  }
}

class App extends StatefulWidget {
  App({Key key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  int _currentIndex = 0;
  List<Widget> _children;

  @override
  void initState() {
    super.initState();
    this._children = [HomeView(), BooksView(), WishesView()];
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _children[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTabTapped,
          currentIndex: _currentIndex,
          selectedItemColor: Colors.amber[800],
          items: [
            BottomNavigationBarItem(
              icon: new Icon(Icons.home),
              label: 'Accueil',
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.book),
              label: 'Livres',
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite), label: 'Souhaits')
          ],
        ));
  }
}
