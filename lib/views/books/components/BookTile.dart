import 'dart:io';

import 'package:flutter/material.dart';
import 'package:my_books/database/models/Book.dart';

class BookTile extends StatelessWidget {
  final Book book;

  BookTile({Key key, this.book}) : super(key: key);

  Widget getMiniature() {
    if (this.book.images == null || this.book.images.length == 0) {
      return Icon(Icons.no_photography);
    } else {
      return Image.file(File(book.images[0].path));
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: SizedBox(width: 64, height: 64, child: getMiniature()),
      title: Text(book.title),
      subtitle: Text(book.author),
    );
  }
}
