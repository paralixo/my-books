import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_books/database/models/Book.dart';
import 'package:my_books/database/models/BookImage.dart';
import 'package:my_books/database/ressources/BookRessource.dart';
import 'package:my_books/views/books/book-form/components/ImageGallery.dart';
import 'package:path/path.dart' as Path;
import 'package:path_provider/path_provider.dart';

class BookFormView extends StatefulWidget {
  final String title;

  BookFormView({Key key, this.title}) : super(key: key);

  @override
  _BookFormViewState createState() => _BookFormViewState();
}

class _BookFormViewState extends State<BookFormView> {
  final _formKey = GlobalKey<FormState>();
  final picker = ImagePicker();

  Book _book = new Book();
  BookRessource _bookRessource = new BookRessource();

  void _validateForm() async {
    _formKey.currentState.save();
    if (_formKey.currentState.validate()) {
      await _bookRessource.saveBook(_book);
      Navigator.pop(context);
    }
  }

  Future saveImages(PickedFile image) async {
    final Directory documentDirectory =
        await getApplicationDocumentsDirectory();
    final File file = File(image.path);

    final String fullPath = Path.join(documentDirectory.path, file.path);

    BookImage bookImage = new BookImage(path: fullPath);
    setState(() {
      if (_book.images == null) _book.images = [];
      _book.images.add(bookImage);
    });
  }

  Future getImage() async {
    // TODO: A adapter pour pouvoir aussi chercher dans les photos du téléphone
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    await saveImages(pickedFile);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: new Form(
              key: _formKey,
              child: SingleChildScrollView(
                  child: Column(
                children: <Widget>[
                  new ListTile(
                    title: const Text('Titre'),
                    subtitle: TextFormField(validator: (String title) {
                      if (title.length == 0)
                        return "Veuillez saisir un titre";
                      else
                        return null;
                    }, onSaved: (String title) {
                      _book.title = title;
                    }),
                  ),
                  new ListTile(
                    title: const Text('Auteur'),
                    subtitle: TextFormField(validator: (String author) {
                      if (author.length == 0)
                        return "Veuillez saisir un auteur";
                      else
                        return null;
                    }, onSaved: (String author) {
                      _book.author = author;
                    }),
                  ),
                  SizedBox(
                      width: 100,
                      height: 100,
                      child: OutlineButton(
                          onPressed: getImage, child: Text("Image"))),
                  ImageGallery(paths: _book.getAllPaths())
                ],
              )))),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          _validateForm();
        },
        tooltip: 'Valider',
        child: new Icon(Icons.check),
        backgroundColor: Colors.green,
      ),
    );
  }
}
