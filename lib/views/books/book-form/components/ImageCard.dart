import 'dart:io';

import 'package:flutter/material.dart';

class ImageCard extends StatelessWidget {
  ImageCard({Key key, @required this.path}) : super(key: key);

  final String path;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(6.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        image: DecorationImage(
          image: Image.file(File(path)).image,
          fit: BoxFit.cover,
        ),
      ),
      child: Positioned(top: 0, right: 0, child: Icon(Icons.help)),
    );
  }
}
