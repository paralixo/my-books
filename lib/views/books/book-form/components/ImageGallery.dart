import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'file:///D:/projets/my_books/my_books/lib/views/books/book-form/components/ImageCard.dart';

class ImageGallery extends StatefulWidget {
  final List<String> paths;

  ImageGallery({Key key, @required this.paths}) : super(key: key);

  @override
  _ImageGalleryState createState() => _ImageGalleryState();
}

class _ImageGalleryState extends State<ImageGallery> {
  List<Widget> imageCards;

  Future<void> generateImages() async {
    imageCards = [];
    for (String path in widget.paths) {
      imageCards.add(ImageCard(path: path));
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: this.generateImages(),
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: Text('...'));
          } else {
            return CarouselSlider(
              items: this.imageCards,
              options: CarouselOptions(
                height: this.imageCards.isEmpty ? 0.0 : 180.0,
                enlargeCenterPage: true,
                autoPlay: true,
                aspectRatio: 16 / 9,
                autoPlayCurve: Curves.fastOutSlowIn,
                enableInfiniteScroll: true,
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                viewportFraction: 0.8,
              ),
            );
          }
        });
  }
}
