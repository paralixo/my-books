import 'package:flutter/material.dart';

import 'book-form/BookForm.dart';

class WishesView extends StatefulWidget {
  final String title = "Ma liste de souhaits";

  WishesView({Key key}) : super(key: key);

  @override
  _WishesViewState createState() => _WishesViewState();
}

class _WishesViewState extends State<WishesView> {
  void _addWish() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
                BookFormView(title: "Ajouter un livre")));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Padding(
            padding: EdgeInsets.all(16.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Bienvenue dans vos souhaits',
                  )
                ],
              ),
            )),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: new FloatingActionButton(
          onPressed: () {
            _addWish();
          },
          tooltip: 'Ajouter un livre',
          child: new Icon(Icons.add),
        ));
  }
}
