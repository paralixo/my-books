import 'package:flutter/material.dart';
import 'package:my_books/database/models/Book.dart';
import 'package:my_books/database/ressources/BookRessource.dart';
import 'file:///D:/projets/my_books/my_books/lib/views/books/components/BookTile.dart';

import 'book-form/BookForm.dart';

class BooksView extends StatefulWidget {
  final String title = "Mes livres";

  BooksView({Key key}) : super(key: key);

  @override
  _BooksViewState createState() => _BooksViewState();
}

class _BooksViewState extends State<BooksView> {
  List<Book> books = [];
  BookRessource _bookRessource = new BookRessource();

  @override
  void initState() {
    this.fetchBooks();
    super.initState();
  }

  void fetchBooks() async {
    List<Book> booksTmp = await _bookRessource.getBooks();
    setState(() {
      books = booksTmp;
    });
  }

  void _addBook() async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
                BookFormView(title: "Ajouter un livre")));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Bienvenue dans vos livres',
              ),
              this.books.isEmpty
                  ? Center(child: Text('Pas de livres'))
                  : SizedBox(
                      height: 400,
                      child: ListView.builder(
                        itemCount: this.books.length,
                        itemBuilder: (context, index) {
                          return BookTile(book: books[index]);
                        },
                      )),
              OutlineButton(onPressed: this.fetchBooks, child: Text("Refresh"))
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          _addBook();
        },
        tooltip: 'Ajouter un livre',
        child: new Icon(Icons.add),
      ),
    );
  }
}
