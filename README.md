# My books

Application qui liste nos livres ainsi que les livres que nous devons acheter pour un passage plus rapide en librairie.

## Pour les développeurs du projet

### Git Hooks

Pour utiliser les hooks du projet, taper la commande suivante dans le répertoire du projet :

```
git config core.hooksPath .githooks/
```