// import 'package:flutter/material.dart';
// import 'package:flutter_test/flutter_test.dart';
// import 'package:my_books/database/constants/tables.constants.dart';
//
// import 'package:my_books/main.dart';
// import 'package:path/path.dart';
// import 'package:sqflite/sqflite.dart';
//
// Future _createDB(Database db) async {
//   await db.execute(
//       "CREATE TABLE $BOOKS_TABLE(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, TITLE TEXT, AUTHOR TEXT);");
//   await db.execute(
//       "CREATE TABLE $BOOKS_IMAGES_TABLE(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, BOOK_ID INTEGER, IMAGE_PATH TEXT);");
// }
//
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('App navigation test', (WidgetTester tester) async {
    expect(1, 1);
  });
//   Database database;
//
//   setUp(() async {
//     database =
//     await openDatabase(join(await getDatabasesPath(), 'database.db'),
//         onCreate: (db, version) async {
//           return await _createDB(db);
//         }, version: 1);
//   });
//
//   testWidgets('App navigation test', (WidgetTester tester) async {
//     // Build our app and trigger a frame.
//     await tester.pumpWidget(MyApp(database));
//
//     // Verify that our page is home.
//     expect(
//         find.ancestor(of: find.text('Accueil'), matching: find.byType(AppBar)),
//         findsOneWidget);
//     expect(
//         find.ancestor(
//             of: find.text('Mes livres'), matching: find.byType(AppBar)),
//         findsNothing);
//     expect(
//         find.ancestor(
//             of: find.text('Ma liste de souhaits'),
//             matching: find.byType(AppBar)),
//         findsNothing);
//
//     // Tap the book icon and trigger a frame.
//     await tester.tap(find.byIcon(Icons.book));
//     await tester.pump();
//
//     // Verify that our new page is books.
//     expect(
//         find.ancestor(of: find.text('Accueil'), matching: find.byType(AppBar)),
//         findsNothing);
//     expect(
//         find.ancestor(
//             of: find.text('Mes livres'), matching: find.byType(AppBar)),
//         findsOneWidget);
//     expect(
//         find.ancestor(
//             of: find.text('Ma liste de souhaits'),
//             matching: find.byType(AppBar)),
//         findsNothing);
//
//     // Tap the favorite icon and trigger a frame
//     await tester.tap(find.byIcon(Icons.favorite));
//     await tester.pump();
//
//     // Verify that our new page is wishes.
//     expect(
//         find.ancestor(of: find.text('Accueil'), matching: find.byType(AppBar)),
//         findsNothing);
//     expect(
//         find.ancestor(
//             of: find.text('Mes livres'), matching: find.byType(AppBar)),
//         findsNothing);
//     expect(
//         find.ancestor(
//             of: find.text('Ma liste de souhaits'),
//             matching: find.byType(AppBar)),
//         findsOneWidget);
//   });
}
